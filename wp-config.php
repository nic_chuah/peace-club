<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'peaceclub' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'sCvG7$hNG*GgkxMg*+nznT{YsdwyfwiEFx/_D7L)/[cP>,h-sbBn`E$nuT$e~?=A' );
define( 'SECURE_AUTH_KEY',  '!!Uzt8mCX5dy[SCAUPt!nja?[$yTq%|hvDy2{{DcY]$]_GI2=<z~P7|IDS{Jq:8N' );
define( 'LOGGED_IN_KEY',    'lE|!)C4*tMLVo^P3]2l?:+.>7-Yr1y?y,S=0~Fk3HP4muB)K6fRb8;1!|U=FJ7HM' );
define( 'NONCE_KEY',        '7{H7ZHT0e{WSTj}{+Uxy@_lp3%&>+&~)#u>`d5Q#nF$6UPOPj_I694!*OncRgTqY' );
define( 'AUTH_SALT',        '1{+8ooKT4$.R>^$SKt*i.BM)zr!A/vN7RFIwtm1<c53yK&$Au]5Li*q7/:0006M#' );
define( 'SECURE_AUTH_SALT', ' >Zp$dZmYa&m=c}xLbM(bEM@kI12/e@0]rij}}4gy>daEf}nweY9GK5h%XlD!/xy' );
define( 'LOGGED_IN_SALT',   '@~$y%A12@_1:GM:LHkIj}n./4i!Zp)s;,7T3X!Zjs),tXD!,GW(2:YPmhTp=0@*T' );
define( 'NONCE_SALT',       '++/w!d)8yW9_-9_a4Go2)1g~O.LmDjv3~)Gw,i$Pal>IFK%:.0f]Yx>ohJ,wL`~j' );
define( 'MYCRED_DEFAULT_LABEL', 'PeacePoints' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
