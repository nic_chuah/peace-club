<?php
/*
Template Name: Test Template
*/
get_header();
?>
<!--<link rel="stylesheet" href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.theme.default.min.css">
<script src="https://owlcarousel2.github.io/OwlCarousel2/assets/vendors/jquery.min.js"></script>
<script src="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/owl.carousel.js"></script> -->

<style>
.container{
  width: 800px;
  margin: 200px auto;
}
h4{
  font-size: 50px;
  color: orange;
}
.bg{
    background: black;
}
.item{background: red;}
.item2{background: yellow;}

@-webkit-keyframes slideInUp {
  from {
    -webkit-transform: translate3d(0, 100%, 0);
    transform: translate3d(0, 100%, 0);
    visibility: visible;
  }

  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

@keyframes slideInUp {
  from {
    -webkit-transform: translate3d(0, 100%, 0);
    transform: translate3d(0, 100%, 0);
    visibility: visible;
  }

  to {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
  }
}

.slideInUp {
  -webkit-animation-name: slideInUp;
  animation-name: slideInUp;
}
</style>

<script>
jQuery(document).ready(function(){

    var owl = jQuery('.owl-carousel');
    owl.owlCarousel({
      items:1,
      autoplaySpeed: 600,
      autoplay: true,
      loop: true,
      autoplayTimeout:3000,
      animateIn: 'slideInUp'
    });
});
</script>
<main id="site-content" role="main">

<div class="container">
	<div class="owl-carousel bg">
    <div class="item"><h4>1</h4></div>
    <div class="item item2"><h4>2</h4></div>
    <div class="item"><h4>3</h4></div>
    <div class="item item2"><h4>4</h4></div>
	</div>
</div>
</main><!-- #site-content -->

<?php get_footer(); ?>
