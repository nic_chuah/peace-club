<?php
	add_action( 'wp_enqueue_scripts', 'tt_child_enqueue_styles' );
	function tt_child_enqueue_styles() {
		 wp_enqueue_style( 'owlcarousel-style', get_stylesheet_directory_uri().'/js/owl-carousel/assets/owl.carousel.min.css' );
	   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
	}

	add_action( 'wp_enqueue_scripts', 'tt_child_enqueue_script' );
	function tt_child_enqueue_script() {
		//wp_enqueue_script( 'jquery-js', 'https://owlcarousel2.github.io/OwlCarousel2/assets/vendors/jquery.min.js' , false );
		wp_enqueue_script( 'owlcarousel-js', get_stylesheet_directory_uri().'/js/owl-carousel/owl.carousel.js', false );
		wp_enqueue_script( 'peace-js', get_stylesheet_directory_uri().'/js/peace.js', false );
	}
	
/*	add_action('admin_footer', 'uncheck_send_user_notification');
	function uncheck_send_user_notification() {
	    $currentScreen = get_current_screen();
	    if ('user' === $currentScreen->id) {
	        echo '<script type="text/javascript">document.getElementById("send_user_notification").checked = false;</script>';
	    }
	} */

	define("FIXED_POINTS", 16.8);
	function calculateLevel($amount, $tier, $levels){
	    $points = $amount * FIXED_POINTS;
	    $tierPoints = $tier * FIXED_POINTS;
	    $perLevelPoints = $tierPoints / $levels;
	    return ceil($points / $perLevelPoints);
	}

	function level($amount){
	    $maxTierVals = array(2000, 5000, 10000, 20000, 40000, 80000, 100000, 150000, 150000, 200000);
	    $level = 0;
	    $calculatedAmount = $amount;
	    if ($amount >= array_sum($maxTierVals) + 100000) {
	        $level = 100;
	    } else {
	        foreach ($maxTierVals as &$tier) {
	            if ($calculatedAmount > 0) {
	                $levels = $tier == 2000 ? 9 : 10;
	                $amount = $calculatedAmount > $tier ? $tier : $calculatedAmount;
	                $level += calculateLevel($amount, $tier, $levels);
	                $calculatedAmount = $calculatedAmount - $tier;
	            }
	        }
	    }
	    return $level;
	}

	function points($amount){
	  $points = round($amount * FIXED_POINTS);
	  return $points;
	}


	add_filter( 'mycred_ranking_row', 'mycredpro_add_avatar_to_leaderboard', 10, 4 );
	function mycredpro_add_avatar_to_leaderboard( $layout, $template, $user, $position ) {

		if ( isset( $user->ID ) )
			$user_id = $user->ID;

		elseif ( isset( $user['ID'] ) )
			$user_id = $user['ID'];

		else return $layout;

		$avatar = get_avatar_url( $user_id, 32 );

		return str_replace('%avatar%', $avatar, $layout);

	}

	add_filter( 'mycred_ranking_row', 'mycredpro_level_to_leaderboard', 10, 4 );
	function mycredpro_level_to_leaderboard( $layout, $template, $user, $position ) {

		if ( isset( $user->ID ) )
			$user_id = $user->ID;

		elseif ( isset( $user['ID'] ) )
			$user_id = $user['ID'];

		else return $layout;

		$amt = $user['cred'];
		$level = level($amt);

		return str_replace('%level%', $level, $layout);

	}

	add_filter( 'mycred_ranking_row', 'mycredpro_horoscope_to_leaderboard', 10, 4 );
	function mycredpro_horoscope_to_leaderboard( $layout, $template, $user, $position ) {

		if ( isset( $user->ID ) )
			$user_id = $user->ID;

		elseif ( isset( $user['ID'] ) )
			$user_id = $user['ID'];

		else return $layout;

		$horoscope = strtolower(get_field('horoscope', 'user_'.$user_id));

		return str_replace('%horoscope%', $horoscope, $layout);

	}

	add_filter( 'mycred_ranking_row', 'mycredpro_age_to_leaderboard', 10, 4 );
	function mycredpro_age_to_leaderboard( $layout, $template, $user, $position ) {

		if ( isset( $user->ID ) )
			$user_id = $user->ID;

		elseif ( isset( $user['ID'] ) )
			$user_id = $user['ID'];

		else return $layout;

		$bday = get_field('birthday', 'user_'.$user_id);
		if($bday) {
			$birthday = new DateTime($bday);
			$interval = $birthday->diff(new DateTime);
		 	$age = $interval->y;
		}

		return str_replace('%age%', $age, $layout);

	}

	add_filter( 'mycred_ranking_row', 'mycredpro_location_to_leaderboard', 10, 4 );
	function mycredpro_location_to_leaderboard( $layout, $template, $user, $position ) {

		if ( isset( $user->ID ) )
			$user_id = $user->ID;

		elseif ( isset( $user['ID'] ) )
			$user_id = $user['ID'];

		else return $layout;

		$location = get_field('location', 'user_'.$user_id);

		return str_replace('%location%', $location, $layout);

	}

	add_filter( 'mycred_ranking_row', 'mycredpro_badge_to_leaderboard', 10, 4 );
	function mycredpro_badge_to_leaderboard( $layout, $template, $user, $position ) {

		if ( isset( $user->ID ) )
			$user_id = $user->ID;

		elseif ( isset( $user['ID'] ) )
			$user_id = $user['ID'];

		else return $layout;

		$badge = get_field('badge', 'user_'.$user_id);
		$bdg = "";
		if ($badge){
				if ($badge == "red"){
					$badgerank = "TOP";
				} elseif ($badge == "gold"){
					$badgerank = "NO.1";
				} elseif ($badge == "silver"){
					$badgerank = "NO.2";
				} elseif ($badge == "bronze"){
					$badgerank = "NO.3";
				}
				$bmonth = get_field('badge_month', 'user_'.$user_id);
				$dateTime = DateTime::createFromFormat("Ymd", $bmonth);
				$month = $dateTime->format('M');
				$bdg = "<div class='badge ".$badge."'>".$badgerank.": ".$month."</div>";
		}

		return str_replace('%badge%', $bdg, $layout);

	}

	add_filter( 'mycred_ranking_row', 'mycredpro_pposition_to_leaderboard', 10, 4 );
	function mycredpro_pposition_to_leaderboard( $layout, $template, $user, $position ) {

		if ( isset( $user->ID ) )
			$user_id = $user->ID;

		elseif ( isset( $user['ID'] ) )
			$user_id = $user['ID'];

		else return $layout;

		$pposition = $position-1;

		return str_replace('%pposition%', $pposition, $layout);

	}

	add_filter( 'mycred_ranking_row', 'mycredpro_levelring_to_leaderboard', 10, 4 );
	function mycredpro_levelring_to_leaderboard( $layout, $template, $user, $position ) {

		if ( isset( $user->ID ) )
			$user_id = $user->ID;

		elseif ( isset( $user['ID'] ) )
			$user_id = $user['ID'];

		else return $layout;

		$amt = get_user_meta( $user_id, 'mycred_default', $single = false );
		$level = level($amt[0]);

		if($level >= 100){
				$levelring = 100;
		} else if($level >=99 ) {
				$levelring = 99;
		} else {
				$levelring = $position-1;
		}

		return str_replace('%levelring%', $levelring, $layout);

	}

	add_filter( 'mycred_ranking_row', 'mycredpro_points_to_leaderboard', 10, 4 );
	function mycredpro_points_to_leaderboard( $layout, $template, $user, $position ) {

		if ( isset( $user->ID ) )
			$user_id = $user->ID;

		elseif ( isset( $user['ID'] ) )
			$user_id = $user['ID'];

		else return $layout;

	 	$amt = $user['cred'];
		$p = points($amt);

		return str_replace('%points%', $p, $layout);

	}
