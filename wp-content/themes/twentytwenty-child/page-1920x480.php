<?php
/*
Template Name: 1920x480 Template
*/
get_header();
?>

<main id="site-content" role="main" class="large-display">	

	<?php
		if (have_posts()) :
			while (have_posts()) :
				the_post();
					the_content();
			endwhile;
		endif;
	?>		
	
</main><!-- #site-content -->

<?php get_footer(); ?>
