<?php
/*
Template Name: Vertical Screen Template
*/
get_header();
?>

<main id="site-content" role="main">
	<div class="header">
		<div class="logo-left"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/peace.gif" /></div>
		<div class="owl-carousel owl-title">
			<div class="item">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/v-monthly-spender.gif" / >
			</div>
			<div class="item">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/v-lifetime-spender.gif" / >
			</div>
			<div class="item">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/v-monthly-giver.gif" / >
			</div>
			<div class="item">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/v-lifetime-giver.gif" / >
			</div>
		</div>
		 <div class="logo-right"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/peace.gif" /></div>
		<div class="clear"></div>
	</div>
	<?php
		if (have_posts()) :
			while (have_posts()) :
				the_post();
					the_content();
			endwhile;
		endif;
	?>
	<div class="clear"></div>
</main><!-- #site-content -->

<?php get_footer(); ?>
