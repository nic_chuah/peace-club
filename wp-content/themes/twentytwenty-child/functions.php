<?php
	add_action( 'wp_enqueue_scripts', 'tt_child_enqueue_styles' );
	function tt_child_enqueue_styles() {
		wp_enqueue_style( 'owlcarousel-style', get_stylesheet_directory_uri().'/js/owl-carousel/assets/owl.carousel.min.css' );		
	   	wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
	}

	add_action( 'wp_enqueue_scripts', 'tt_child_enqueue_script' );
	function tt_child_enqueue_script() {
		wp_enqueue_script( 'owlcarousel-js', get_stylesheet_directory_uri().'/js/owl-carousel/owl.carousel.js', array('jquery') );
		wp_enqueue_script( 'peace-js', get_stylesheet_directory_uri().'/js/peace.js', false );
	}

	add_action('admin_footer', 'uncheck_send_user_notification');
	function uncheck_send_user_notification() {
	    $currentScreen = get_current_screen();
	    if ('user' === $currentScreen->id) {
	        echo '<script type="text/javascript">document.getElementById("send_user_notification").checked = false;</script>';
	    }
	}

	define("FIXED_POINTS", 16.8);

	function calculateLevel($amount, $tier, $levels){
	    $points = $amount * FIXED_POINTS;
	    $tierPoints = $tier * FIXED_POINTS;
	    $perLevelPoints = $tierPoints / $levels;
	    return ceil($points / $perLevelPoints);
	}

	function level($amount){
    switch(true) {
        case ($amount <= 2000):
            return calculateLevel($amount, 2000, 9);
        case ($amount <= 7000):
            return 9 + calculateLevel(($amount - 2000), 5000, 10);
        case ($amount <= 17000):
            return 19 + calculateLevel(($amount - 7000), 10000, 10);
        case ($amount <= 37000):
            return 29 + calculateLevel(($amount - 17000), 20000, 10);
        case ($amount <= 77000):
            return 39 + calculateLevel(($amount - 37000), 40000, 10);
        case ($amount <= 157000):
            return 49 + calculateLevel(($amount - 77000), 80000, 10);
        case ($amount <= 257000):
            return 59 + calculateLevel(($amount - 157000), 100000, 10);
        case ($amount <= 407000):
            return 69 + calculateLevel(($amount - 257000), 150000, 10);
        case ($amount <= 557000):
            return 79 + calculateLevel(($amount - 407000), 150000, 10);
        case ($amount <= 757000):
            return 89 + calculateLevel(($amount - 557000), 200000, 10);
        case ($amount < 857000):
            return 99;
        case ($amount >= 857000):
            return 100;
    }
	}

	function points($amount){
	  $points = round($amount * FIXED_POINTS);
	  return $points;
	}


	add_filter( 'mycred_ranking_row', 'mycredpro_add_avatar_to_leaderboard', 10, 4 );
	function mycredpro_add_avatar_to_leaderboard( $layout, $template, $user, $position ) {

		if ( isset( $user->ID ) )
			$user_id = $user->ID;

		elseif ( isset( $user['ID'] ) )
			$user_id = $user['ID'];

		else return $layout;

		$avatar = get_avatar_url( $user_id, 32 );

		return str_replace('%avatar%', $avatar, $layout);

	}

	add_filter( 'mycred_ranking_row', 'mycredpro_level_to_leaderboard', 10, 4 );
	function mycredpro_level_to_leaderboard( $layout, $template, $user, $position ) {

		if ( isset( $user->ID ) )
			$user_id = $user->ID;

		elseif ( isset( $user['ID'] ) )
			$user_id = $user['ID'];

		else return $layout;

		$amt = get_user_meta($user_id, 'mycred_default', false);		
		$level = (level($amt[0]) == 0)? 1: level($amt[0]);				

		return str_replace('%level%', $level, $layout);

	}

	add_filter( 'mycred_ranking_row', 'mycredpro_horoscope_to_leaderboard', 10, 4 );
	function mycredpro_horoscope_to_leaderboard( $layout, $template, $user, $position ) {

		if ( isset( $user->ID ) )
			$user_id = $user->ID;

		elseif ( isset( $user['ID'] ) )
			$user_id = $user['ID'];

		else return $layout;

		$horoscope = strtolower(get_field('horoscope', 'user_'.$user_id));

		return str_replace('%horoscope%', $horoscope, $layout);

	}

	add_filter( 'mycred_ranking_row', 'mycredpro_age_to_leaderboard', 10, 4 );
	function mycredpro_age_to_leaderboard( $layout, $template, $user, $position ) {

		if ( isset( $user->ID ) )
			$user_id = $user->ID;

		elseif ( isset( $user['ID'] ) )
			$user_id = $user['ID'];

		else return $layout;

		$bday = get_field('birthday', 'user_'.$user_id);		
		if($bday) {
			$birthday = new DateTime($bday);
			$interval = $birthday->diff(new DateTime);
		 	$age = $interval->y;
		}

		return str_replace('%age%', $age, $layout);

	}

	add_filter( 'mycred_ranking_row', 'mycredpro_location_to_leaderboard', 10, 4 );
	function mycredpro_location_to_leaderboard( $layout, $template, $user, $position ) {

		if ( isset( $user->ID ) )
			$user_id = $user->ID;

		elseif ( isset( $user['ID'] ) )
			$user_id = $user['ID'];

		else return $layout;

		$location = get_field('location', 'user_'.$user_id);

		return str_replace('%location%', $location, $layout);

	}

	add_filter( 'mycred_ranking_row', 'mycredpro_display_birthday_to_leaderboard', 10, 4 );
	function mycredpro_display_birthday_to_leaderboard( $layout, $template, $user, $position ) {

		if ( isset( $user->ID ) )
			$user_id = $user->ID;

		elseif ( isset( $user['ID'] ) )
			$user_id = $user['ID'];

		else return $layout;

		$showbirthday = (get_field('show_birthday', 'user_'.$user_id) == 1)? '':"style='display:none;'";		

		return str_replace('%showbirthday%', $showbirthday, $layout);

	}

	add_filter( 'mycred_ranking_row', 'mycredpro_badge_to_leaderboard', 10, 4 );
	function mycredpro_badge_to_leaderboard( $layout, $template, $user, $position ) {

		if ( isset( $user->ID ) )
			$user_id = $user->ID;

		elseif ( isset( $user['ID'] ) )
			$user_id = $user['ID'];

		else return $layout;

		$badge = get_field('badge', 'user_'.$user_id);
		$bdg = "";
		if ($badge){
				if ($badge == "red"){
					$badgerank = "TOP";
				} elseif ($badge == "gold"){
					$badgerank = "NO.1";
				} elseif ($badge == "silver"){
					$badgerank = "NO.2";
				} elseif ($badge == "bronze"){
					$badgerank = "NO.3";
				}
				$bmonth = get_field('badge_month', 'user_'.$user_id);
				$dateTime = DateTime::createFromFormat("Ymd", $bmonth);
				$month = $dateTime->format('M');
				$bdg = "<div class='badge ".$badge."'>".$badgerank.": ".$month."</div>";
		}

		return str_replace('%badge%', $bdg, $layout);

	}

	add_filter( 'mycred_ranking_row', 'mycredpro_coins_to_leaderboard', 10, 4 );
	function mycredpro_coins_to_leaderboard( $layout, $template, $user, $position ) {

		if ( isset( $user->ID ) )
			$user_id = $user->ID;

		elseif ( isset( $user['ID'] ) )
			$user_id = $user['ID'];

		else return $layout;

		$threekcoins = get_field('3k_coins_times', 'user_'.$user_id);
		$fivekcoins = get_field('5k_coins_times', 'user_'.$user_id);		
		$ninekcoins = get_field('99k_coins_times', 'user_'.$user_id);		
				
		if($threekcoins){			
			$threetimes = ($threekcoins < 10)?"x":"";		
			$cns .= "<div class='coins coins3k'><span>".$threetimes.$threekcoins."</span></div>";
		} 
		
		if ($fivekcoins){			
			$fivetimes = ($fivekcoins < 10)?"x":"";		
			$cns .= "<div class='coins coins5k'><span>".$fivetimes.$fivekcoins."</span></div>";
		}

		if ($ninekcoins){			
			$ninetimes = ($ninekcoins < 10)?"x":"";		
			$cns .= "<div class='coins coins9k'><span>".$ninetimes.$ninekcoins."</span></div>";
		}

		/*if ($coins){
				if ($coins == "3k"){
					$coinsrank = "coins3k";
				} elseif ($coins == "5k"){
					$coinsrank = "coins5k";
				} 
				$coinsrank."<br />";
				$ctimes = get_field('coins_times', 'user_'.$user_id);	
				$times = ($ctimes < 10)?"x":"";			
				$cns = "<div class='coins ".$coinsrank."'><span>".$times.$ctimes."</span></div>";
		} */

		return str_replace('%coins%', $cns, $layout);

	}

	add_filter( 'mycred_ranking_row', 'mycredpro_pposition_to_leaderboard', 10, 4 );
	function mycredpro_pposition_to_leaderboard( $layout, $template, $user, $position ) {

		if ( isset( $user->ID ) )
			$user_id = $user->ID;

		elseif ( isset( $user['ID'] ) )
			$user_id = $user['ID'];

		else return $layout;

		$pposition = $position-1;

		return str_replace('%pposition%', $pposition, $layout);

	}


	// add_filter( 'mycred_ranking_row', 'mycredpro_toplevelring_to_leaderboard', 10, 4 );
	// function mycredpro_toplevelring_to_leaderboard( $layout, $template, $user, $position ) {

	// 	if ( isset( $user->ID ) )
	// 		$user_id = $user->ID;

	// 	elseif ( isset( $user['ID'] ) )
	// 		$user_id = $user['ID'];

	// 	else return $layout;

	// 	$amt = get_user_meta( $user_id, 'mycred_default', $single = false );
	// 	$level = level($amt[0]);

	// 	if($level >= 100){
	// 			$toplevelring = 100;
	// 	} else if($level >=99 ) {
	// 			$toplevelring = 99;
	// 	} else {
	// 		if($position % 2 == 0){
	// 				$toplevelring = "even";
	// 		}
	// 		else{
	// 				$toplevelring = "odd";
	// 		}
	// 	}

	// 	return str_replace('%toplevelring%', $toplevelring, $layout);

	// }

	add_filter( 'mycred_ranking_row', 'mycredpro_toplifetimering_to_leaderboard', 10, 4 );
	function mycredpro_toplifetimering_to_leaderboard( $layout, $template, $user, $position ) {

		if ( isset( $user->ID ) )
			$user_id = $user->ID;

		elseif ( isset( $user['ID'] ) )
			$user_id = $user['ID'];

		else return $layout;

		$amt = get_user_meta( $user_id, 'mycred_default', $single = false );
		$level = level($amt[0]);		
		
		if (strlen($level) >= 2){
			$ring = ($level ==99)? 99 : substr($level, 0, -1);		
		} else {
			$ring = 0;
		}		
		//$ring = 0;
		if ($ring == 10){
			$toplifetimering = 100;
		} else if ($ring == 99 ) {
			$toplifetimering = 99;
		} else if ($ring == 9 ) {
			$toplifetimering = 9;	
		} else if ($ring == 8){
			$toplifetimering = 8;
		} else if ($ring == 7){
			$toplifetimering = 7;
		} else if ($ring == 6){
			$toplifetimering = 6;
		} else if ($ring == 5){
			$toplifetimering = 5;
		} else if ($ring == 4){
			$toplifetimering = 4;
		} else if ($ring == 3){
			$toplifetimering = 3;
		} else if ($ring == 2){
			$toplifetimering = 2;
		} else if ($ring == 1){
			$toplifetimering = 1;
		} else {
			$toplifetimering = 0;
		}

		return str_replace('%toplifetimering%', $toplifetimering, $layout);

	}

	// add_filter( 'mycred_ranking_row', 'mycredpro_hlevelring_to_leaderboard', 10, 4 );
	// function mycredpro_hlevelring_to_leaderboard( $layout, $template, $user, $position ) {

	// 	if ( isset( $user->ID ) )
	// 		$user_id = $user->ID;

	// 	elseif ( isset( $user['ID'] ) )
	// 		$user_id = $user['ID'];

	// 	else return $layout;

	// 	$amt = get_user_meta( $user_id, 'mycred_default', $single = false );
	// 	$level = level($amt[0]);

	// 	if ($level < 99){
	// 		$hlevelring = $position-1;
	// 	} else if($level >= 100){
	// 			$hlevelring = 100;
	// 	} else if($level >=99 ) {
	// 			$hlevelring = 99;
	// 	} /*else {
	// 			echo $hlevelring = $position-1;
	// 	}*/

	// 	return str_replace('%hlevelring%', $hlevelring, $layout);

	// }

	// add_filter( 'mycred_ranking_row', 'mycredpro_levelring_to_leaderboard', 10, 4 );
	// function mycredpro_levelring_to_leaderboard( $layout, $template, $user, $position ) {

	// 	if ( isset( $user->ID ) )
	// 		$user_id = $user->ID;

	// 	elseif ( isset( $user['ID'] ) )
	// 		$user_id = $user['ID'];

	// 	else return $layout;

	// 	$amt = get_user_meta( $user_id, 'mycred_default', $single = false );
	// 	$level = level($amt[0]);

	// 	if($level >= 100){
	// 			$levelring = 100;
	// 	} else if($level >=99 ) {
	// 			$levelring = 99;
	// 	} else {
	// 			$levelring = $position-1;
	// 	}

	// 	return str_replace('%levelring%', $levelring, $layout);

	// }

	add_filter( 'mycred_ranking_row', 'mycredpro_points_to_leaderboard', 10, 4 );
	function mycredpro_points_to_leaderboard( $layout, $template, $user, $position ) {

		if ( isset( $user->ID ) )
			$user_id = $user->ID;

		elseif ( isset( $user['ID'] ) )
			$user_id = $user['ID'];

		else return $layout;

	 	$amt = $user['cred'];
		$p = points($amt);

		return str_replace('%points%', $p, $layout);

	}

// This will suppress empty email errors when submitting the user form
add_action('user_profile_update_errors', 'my_user_profile_update_errors', 10, 3 );
function my_user_profile_update_errors($errors, $update, $user) {
    $errors->remove('empty_email');
}

// This will remove javascript required validation for email input
// It will also remove the '(required)' text in the label
// Works for new user, user profile and edit user forms
add_action('user_new_form', 'my_user_new_form', 10, 1);
add_action('show_user_profile', 'my_user_new_form', 10, 1);
add_action('edit_user_profile', 'my_user_new_form', 10, 1);
function my_user_new_form($form_type) {
    ?>
    <script type="text/javascript">
        jQuery('#email').closest('tr').removeClass('form-required').find('.description').remove();
        // Uncheck send new user email option by default
        <?php if (isset($form_type) && $form_type === 'add-new-user') : ?>
            jQuery('#send_user_notification').removeAttr('checked');
        <?php endif; ?>
    </script>
    <?php
}