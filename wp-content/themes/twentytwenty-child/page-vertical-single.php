<?php
/*
Template Name: Vertical Screen Template - Single
*/
get_header();
?>

<main id="site-content" role="main">
	<div class="header">
		<div class="logo-left"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/peace.png" /></div>		
		<div class="header-title">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/monthly-top-spender.png" / >			
		</div>
		 <div class="logo-right"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/peace.png" /></div>
		<div class="clear"></div>
	</div>

	<?php
		if (have_posts()) :
			while (have_posts()) :
				the_post();
					the_content();
			endwhile;
		endif;
	?>
	
	<div class="clear"></div>
</main><!-- #site-content -->

<?php get_footer(); ?>
