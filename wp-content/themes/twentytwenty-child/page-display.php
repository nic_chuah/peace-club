<?php
/*
Template Name: Display Screen Template
*/
get_header();
global $post;
?>
<?php
	if (have_posts()) :
		while (have_posts()) :
			the_post();				
				$j = 0;
				if(get_field('empty_leaderboard') == 1){ 		
					$url .= "'http://peaceclub.local/vertical-empty-peace-club'";
					$url .= ', ';
					$j++;
				} 

				if(get_field('lifetime_top_spender') == 1){ 		
					$url .= "'http://peaceclub.local/vertical-lifetime-top-spender'";
					$url .= ', ';
					$j++;
				} 
				
				if(get_field('monthly_top_receiver') == 1){ 
					$url .= "'http://peaceclub.local/vertical-monthly-top-receiver'";
					$url .= ', ';
					$j++;
				} 
				
				if (get_field('monthly_top_spender') == 1) {
					$url .= "'http://peaceclub.local/vertical-monthly-top-spender'";
					$url .= ', ';
					$j++;
				}	

				if ($url){
					$url = rtrim($url, ', ');
					$last = str_replace("'","",substr($url, strrpos($url, ',') + 1));
				} 
		endwhile;
	endif;	
	?>

<?php if(url){ ?>
<script>
jQuery(document).ready(function(){	
	
	var url = [<?php echo $url; ?>];
	var i = 0;

	setInterval(function() {				
		jQuery('#vscreen').attr('src', url[i % <?php echo $j; ?>]); 
		i++;		
	}, 60000);	
});
</script>
<?php } ?>

<main id="site-content" role="main">	

	<iframe id="vscreen" src="<?php echo $last; ?>" width="1080" height="1920"></iframe>					

</main><!-- #site-content -->

<?php get_footer(); ?>
