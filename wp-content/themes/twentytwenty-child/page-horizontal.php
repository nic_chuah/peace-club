<?php
/*
Template Name: Horizontal Screen Template
*/
get_header();
?>

<main id="site-content" role="main" class="horizontal-screen">

		<div class="horizontal-rank">
		<?php
			if (have_posts()) :
				 while (have_posts()) :
						the_post();
							 the_content();
				 endwhile;
			endif;
		?>
		</div>		

		<div class="h-title">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/h-monthly-top-spender.png">
		</div

</main><!-- #site-content -->

<?php get_footer(); ?>
