jQuery(document).ready(function () {
  /*var otitle = jQuery(".owl-title");
  otitle.owlCarousel({
    items: 1,
    autoplaySpeed: 600,
    autoplay: false,
    loop: true,
    autoplayTimeout: 23900,
    mouseDrag: true,
  });*/

  var top = jQuery(".owl-top");
  top.owlCarousel({
    items: 1,
    autoplaySpeed: 600,
    autoplay: false,
    loop: true,
    autoplayTimeout: 10000,
    mouseDrag: true,
  });

  var rank = jQuery(".owl-rank");
  rank.owlCarousel({
    items: 1,
    autoplaySpeed: 600,
    autoplay: false,
    loop: true,
    autoplayTimeout: 10000,
    mouseDrag: true,
    animateIn: "slideInUp",
  });

  var rank = jQuery(".owl-large");
  rank.owlCarousel({
    items: 1,
    autoplaySpeed: 600,
    autoplay: false,
    loop: true,
    autoplayTimeout: 10000,
    mouseDrag: true,
  });
});
