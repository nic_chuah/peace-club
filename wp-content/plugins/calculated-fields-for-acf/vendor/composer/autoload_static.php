<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitd75ce7b862c49976ead1ff90b383f6ee
{
    public static $prefixLengthsPsr4 = array (
        'C' => 
        array (
            'CalculatedFields\\' => 17,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'CalculatedFields\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitd75ce7b862c49976ead1ff90b383f6ee::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitd75ce7b862c49976ead1ff90b383f6ee::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
